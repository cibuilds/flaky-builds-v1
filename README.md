# Ontology to detect flaky builds (v1)

This code repository hosts ontology that is used to detect flaky builds.

## Files

- build.ttl: the ontology in ttl format
- build_example.ttl: a knowledge graph that represents a build along with its metadata
- URI_patterns.md: documentation about URI design (i.e., how URIs are generated)
- Detecting flaky builds using PyKeen.ipynb: notebook of the code used to compute performance
