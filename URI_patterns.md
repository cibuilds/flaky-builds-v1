URI patterns for Build data
===========================

This file documents how the URI of individuals are generated for the Build data.  
sha1() represents the SHA-1 function defined in [RFC6194](https://datatracker.ietf.org/doc/html/rfc6194).


### Build

```turtle
http://purl.org/cibuilds/build/sha1(<build_uid>)
# e.g. http://purl.org/cibuilds/build/a6be6637d1e10cb2d4209860e29bcff134ebd2d8
```


### Job

```turtle
http://purl.org/cibuilds/job/sha1(<job_uid>)
# e.g. http://purl.org/cibuilds/job/13e151e642ca258d4314333fa34f77a0d81fea12
```


### BuildMachine

```turtle
http://purl.org/cibuilds/machine/sha1(<hostname>)
# e.g. http://purl.org/cibuilds/machine/8eaece78794e6f7de8aedd826c85837ce334e0d6
```


### SourceCodeUpdate / TestCodeUpdate / ConfigUpdate

```turtle
http://purl.org/cibuilds/update/sha1(<update_checksum>)
# e.g. http://purl.org/cibuilds/update/97a01f0cf844ee482f02e731dad3b5a4b9f9725a
```
where update_checksum is
```python
update_checksum = f"{job_uid}:" + ";".join(sorted(categories[file_type]))
```
where `categories[file_type]` is a list of relative filepaths (e.g., config.xml) of type file_type (i.e., SRC, TESTS, CONFIG) modified between previous build and this build (i.e., build triggered because of code changes).


### CodeRepo
```turtle
http://purl.org/cibuilds/code-repository/sha1(<server>:<project_name>:<repo_name>)
# e.g. http://purl.org/cibuilds/code-repository/a9449e4e82fdd7d695644b9747e017962dc5bdd7
```
where `<server>`, `<project_name>` and `<repo_name>` are code repository identifiers.


### SourceFile

```turtle
http://purl.org/cibuilds/file/sha1(<job_uid>:<relative_path>)
# e.g. http://purl.org/cibuilds/file/bdba510b50bc270e07a50f61f7a383b901185f61
```
where `<relative_path>` is the path to the file relative to the root of the git repository (e.g., src/main.py)


### Users / Timers

```turtle
http://purl.org/cibuilds/user/sha1(<user_id>)
http://purl.org/cibuilds/timer/<start_hour>h
```
where <start_hour> is the hour (UTC) where the build has started (i.e., execution of the timer)


### RootCause

```turtle
http://purl.org/cibuilds/root-cause/{EXTERNAL,REPO,SRC,TESTS,CONFIG,SRC_TESTS,VARIOUS}
# e.g. http://purl.org/cibuilds/root-cause/EXTERNAL
```


### BuildStage

```turtle
http://purl.org/cibuilds/stage/sha1(<job_uid>:<stage_name>)
# e.g. http://purl.org/cibuilds/stage/fbfb671404d6f387545768789ecb9c7cb5f9c09a
```


### Command

```turtle
http://purl.org/cibuilds/command/sha1(<job_uid>:<command_name>)
# e.g. http://purl.org/cibuilds/command/8ddf33303d8a35156d3a8b9f353c158be9980960
```
